/* vox.c
 *
 * Copyright (C) 2018 J Rain De Jager
 * this program is free software: you can redistribute and/or modify it
 * under the terms of the GNU GPL, version 2 or later
 * this program is published with ABSOLUTELY NO WARRENTY
 * the GNU GPL can be found at www.gnu.org/licenses
 *
 * speach synthesis pipeline
 * compile command:
 *   gcc $CFLAGS -o vox vox.c -lreadline -lespeak
 * uses readline to make it easier to work with
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <espeak/speak_lib.h>

char* voice;

void loop (void) {
	// main loop of speaking
	char* line;
	line = readline("vox: ");
	add_history(line);
	if (line != NULL) {
		while (espeak_Synth(line,
				strlen(line),
				0,
				POS_CHARACTER,
				0,
				espeakCHARS_UTF8,
				NULL,
				NULL) == EE_BUFFER_FULL) {
			// wait for the buffer to empty
		}
		free(line);
		loop(); // recursion (avoids goto)
	}
}

int main (void) {
	// initialization
	espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
	espeak_SetParameter(espeakCAPITALS, 10, 0);
	if ((voice = getenv("VOXVOICE")) == NULL) {
		voice = "en";
	}
	espeak_SetVoiceByName(voice);
	loop();
	return 0;
}
