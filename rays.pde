/* a class to get random numbers within a certain range
 */
class RandRange {
  float minimum; // lower bound for number generation
  float maximum; // upper bound for number generation
  
  RandRange(float min, float max) {
    minimum = min;
    maximum = max;
  }
  
  float sample() {
    return random(minimum, maximum);
  }
  
}

// parameter ranges
RandRange n_joints; // number of joints in a spine
RandRange sway_freq_time; // the frequency of the swaying in terms of time
RandRange sway_freq_y; // the frequency of the sway in terms of y value
RandRange sway_amp; // the amplitude of the sway
RandRange segment_len; // the length of each segment
RandRange n_tail_joints; // number of joints in the tail
RandRange delta_width; // the change in width with a new joint
RandRange fill_r, fill_g, fill_b; // fill color in red, green, and blue

// generated variables
int jointnum; // the number of joints (number of segments + 1)
int tailnum; // number of joints in the tail
float seglengths[]; // the length of each segment
float widths[]; // the width of each segment
float swayfreqt; // sway frequency by time
float swayfreqy; // sway frequency by y
float swayamp; // sway amplitude
color fillc; // fill color

// "cache" variables (can be computed with generated variables, just for conviene
float y_start; // the y value of the highest joint

void rand_seglen() {
  // randomize the seglengths, tailnum, and jointnum variables
  jointnum = int(n_joints.sample());
  tailnum = int(n_tail_joints.sample());
  seglengths = new float[jointnum + tailnum];
  float total_length = 0.0;
  int i;
  for (i = 0; i < jointnum; i++) { // body segments
    seglengths[i] = segment_len.sample();
    total_length += seglengths[i];
  }
  for (; i < jointnum + tailnum; i++) { // tail segments (don't count in total length as much)
    seglengths[i] = segment_len.sample();
    total_length += seglengths[i] / 2;
  }
  y_start = (height - total_length) / 2;
}

void rand_widths() {
  // randomize the widths variable
  widths = new float[jointnum + 1];
  widths[0] = 0;
  int i;
  for (i = 1; i < jointnum / 2; i++) { // expanding half
    println(i);
    widths[i] = widths[i - 1] + delta_width.sample() * 1.5;
  }
  for (; i < jointnum; i++) { // contracting half
    widths[i] = widths[i - 1] - delta_width.sample() * 0.5;
  }
  widths[jointnum] = 0;
}

void rand_drawing() {
  // randomize the drawing variables
  swayfreqt = sway_freq_time.sample();
  swayfreqy = sway_freq_y.sample();
  swayamp = sway_amp.sample();
  fillc = color(fill_r.sample(), fill_g.sample(), fill_b.sample());
}

void setup() {
  size(1000, 1000, P3D);
  n_joints = new RandRange(5, 9);
  n_tail_joints = new RandRange(2, 4);
  sway_freq_time = new RandRange(0.5, 1.5);
  sway_freq_y = new RandRange(0.003, 0.01);
  sway_amp = new RandRange(20, 50);
  segment_len = new RandRange(50, 100);
  delta_width = new RandRange(50, 120);
  fill_r = new RandRange(20, 190);
  fill_g = new RandRange(5, 100);
  fill_b = new RandRange(40, 250);
  rand_seglen();
  rand_widths();
  rand_drawing();
}

void draw_outline() {
  // draw the outline and fill of the ray
  fill(fillc);
  float y_old = y_start;
  float x_old = swayamp * sin(swayfreqt * float(millis()) / 1000 + swayfreqy * y_old) + width / 2;
  for (int i = 1; i < jointnum + 1; i++) {
    float y_new = y_old + seglengths[i];
    float x_new = swayamp * sin(swayfreqt * float(millis()) / 1000 + swayfreqy * (y_new + seglengths[i])) + width / 2;
    stroke(fillc);
    strokeWeight(1.5);
    quad(x_old + widths[i - 1], y_old, x_new + widths[i], y_new, x_new - widths[i], y_new,  x_old - widths[i - 1], y_old);
    stroke(0, 0, 0);
    strokeWeight(4.0);
    line(x_old + widths[i - 1], y_old, x_new + widths[i], y_new);
    line(x_old - widths[i - 1], y_old, x_new - widths[i], y_new);
    y_old = y_new;
    x_old = x_new;
  }
}

void draw_spine() {
  // draw the spine of the ray
  strokeWeight(1.0);
  float y_old = y_start;
  float x_old = swayamp * sin(swayfreqt * float(millis()) / 1000 + swayfreqy * y_old) + width / 2;
  int i;
  for (i = 0; i < jointnum; i++) {
    float y_new = y_old + seglengths[i];
    float x_new = swayamp * sin(swayfreqt * float(millis()) / 1000 + swayfreqy * (y_new + seglengths[i])) + width / 2;
    line(x_old, y_old, x_new, y_new);
    y_old = y_new;
    x_old = x_new;
  }
  strokeWeight(4.5);
  for (; i < jointnum + tailnum; i++) {
    float y_new = y_old + seglengths[i];
    float x_new = swayamp * sin(swayfreqt * float(millis()) / 1000 + swayfreqy * (y_new + seglengths[i])) + width / 2;
    line(x_old, y_old, x_new, y_new);
    y_old = y_new;
    x_old = x_new;
  }
}

void draw() {
  background(#B9F9FF);
  draw_outline();
  draw_spine();
}
